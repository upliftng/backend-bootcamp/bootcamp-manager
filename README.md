# bootcamp-manager

Welcome to the Uplift NG Bootcamp Manager Project.

## Features
- Manage Tracks
- Track user progress
- Allow adding links to Git Commits create by users
- Remind users of tasks and deadlines
- Motivations
- Manage Documentations and Resources
- Integration with slack